Настройка виртуальной машины на Linux для работы с node.js
===

1. Сначала скачаем git
Для установки Git с помощью пакетного менеджера. сначала обновим списки пакетов из репозиториев:
```
sudo apt update
```
Затем осталось загрузить и установить программу:

 sudo apt install git

везде писать "y" на все вопросы программы.

в случае если будет писать "Abort."
значит вначале установки у вас был установлен русский язык, поставьте язык на английский сразу же .

2)Установка node

Теперь давайте удалим node если она у нас имеется:

 sudo apt-get remove --purge node

Теперь можно переходить к рассмотрению того как установить Node js Ubuntu 16.04.

Для установки node с помощью NVM нам нужен компилятор С++
 sudo apt-get install build-essential checkinstall

Также нам понадобится libssl:
 sudo apt-get install libssl-dev

везде писать "y" на все вопросы программы.

в случае если будет писать "Abort."
значит вначале установки у вас был установлен русский язык, поставьте язык на английский сразу же.

Скачать и установить менеджер версий NVM можно с помощью следующей команды:

 curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh | bash

перезагрузите ваш терминал для того, чтобы команда NVM работала исправно (без перезгрузки терминала вам будет писать о том, что данная комнда не найдена)

 nvm install 6.0
где nvm - node version manager, install - ключ команды отвечающий за установку,6.0 - номер версии которая вам нужна(указывайте последнюю, если не хотите накосячить и не знаете разницы,посмотреть версию можно тут https://nodejs.org/en/, на край ту на которой работаете)
 nvm use 6.0
где nvm - node version manager, use - ключ, отвечающий за то какую версию node вы будите использовать, 6.0 - версия, которую вы хотите использовать из уже скачаных. 
 node -v выведет вам номер версии и скажет тем самым что у вас все установилось

3)Установка nano 
установка
sudo apt-get install nano
работа
CTRL + O = сохранение
CTRL + A = Перейти к началу строки.
CTRL + E = Перейти к концу строки.
CTRL + Y = Прокрутить страницу вниз.
CTRL + V = Прокрутить страницу вверх.

4)установка и использование mongodb:
mongodb программа очень непостоянная и название ее меняется очень часто, если какая-то команда пишеть вам ошибку попробуйте слово "mongodb" заменить на "mongod" или "mongo"
Установка
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
успешный ответ на комндау:
gpg: Total number processed: 1
gpg:               imported: 1  (RSA: 1)

echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
sudo apt-get update
sudo apt-get install -y mongodb-org
зайти в файл
sudo nano /etc/systemd/system/mongodb.service
написать в нем следующее: 
[Unit]
Description=High-performance, schema-free document-oriented database
After=network.target

[Service]
User=mongodb
ExecStart=/usr/bin/mongod --quiet --config /etc/mongod.conf

[Install]
WantedBy=multi-user.target


Запуск
 sudo systemctl start mongodb
Проверка работы:
 sudo systemctl status mongodb

Список сайтов, с которых была взята инфа, для ознакомления и освоения
1)https://losst.ru/ustanovka-git-ubuntu-16-04
2)https://losst.ru/ustanovka-node-js-ubuntu-16-04
3)https://www.hostinger.ru/rukovodstva/kak-ustanovit-tekstoviy-redaktor-linux-nano
4)https://www.digitalocean.com/community/tutorials/mongodb-ubuntu-16-04-ru


